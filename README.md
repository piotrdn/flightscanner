# FlightScanner

This application was written in Java 17 and Spring Boot 3.0.5.
All used dependencies are in pom.xml file.

## Getting started

This consol application takes input parameters: 
1) date of flight from A to B location
2) date of flight from B to A location
3) code of airport A
4) code of airport B
5) number of days for holidays

Based on tequila kiwi API https://tequila.kiwi.com/portal/docs/tequila_api/search_api application get the information about flights. For example price and availability for tickets.

The main function is to put the period for holidays, for example from 01/07/2023 to 14/07/2023
and defining how long is the vacation, for example, 2 days. Algorithm counts the number of possible options and connects to the external API to finds the sum of the lowest price for the flight there and back.

For example:

Input:\
######################################################\
########## Flight Scanner app ##########\
Put the date TO [DD/MM/YYYY]: \
01/07/2023\
Put the date FROM [DD/MM/YYYY]: \
14/07/2023\
Fly from:\
GDN\
Fly to:\
WRO\
Holidays number of days: \
2

Result:\
Number of days: 14

Option number: 1\
https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO ...\
{"search_id": "122aec92-18ca-4c43-59ab-4231d69710f0", "currency": "EUR", ...\
There is lack of flights for this day!
Date TO: 01/07/2023\
https://api.tequila.kiwi.com/v2/search?fly_from=WRO&fly_to=GDN ...\
{"search_id": "3a1b6fc5-22f5-7820-9a9f-a58532e46474", "currency": "EUR", ...\
There is lack of flights for this day!\
Date FROM: 03/07/2023\
Sum 1: 0.0

Option number: 2\
https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO ...\
{"search_id": "32b650ab-e6a6-ee17-71ff-cd5883805922", "currency": "EUR", ...\
Price GDN -> WRO 37.0 (26.27)\
Date TO: 02/07/2023\
https://api.tequila.kiwi.com/v2/search?fly_from=WRO&fly_to=GDN  ...\
{"search_id": "3b568dee-77af-e5b5-8a17-af922e3129a1", "currency": "EUR", ...\
Price WRO -> GDN 28.0 (19.88)\
Date FROM: 04/07/2023\
Sum 2: 65.0


Option number: 3\
https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO ...\
{"search_id": "c4553bd5-63f0-96cd-f3bb-f10762950e61", "currency": "EUR", ...\
There is lack of flights for this day!\
Date TO: 03/07/2023\
https://api.tequila.kiwi.com/v2/search?fly_from=WRO&fly_to=GDN ...\
{"search_id": "33cace74-17b0-ea5e-aab3-278321a4c364", "currency": "EUR",  ...\
There is lack of flights for this day!\
Date FROM: 05/07/2023\
Sum 3: 0.0


Option number: 4\
https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO ...\
{"search_id": "c098f63a-9ab4-d23a-fecb-8b2a3224be10", "currency": "EUR", ...\
Price GDN -> WRO 28.0 (19.88)\
Date TO: 04/07/2023\
https://api.tequila.kiwi.com/v2/search?fly_from=WRO&fly_to=GDN ...\
{"search_id": "6ba27848-4e41-ef7d-d609-354b67f8ff1e", "currency": "EUR", ...\
Price WRO -> GDN 23.0 (16.33)\
Date FROM: 06/07/2023\
Sum 4: 51.0


Option numer: 5\
...

Option numer: 6\
...

etc


Sum of prices (TO + FROM) -> Set number 1 is: 0.0 (0.0)  ->  TO: 01/07/2023 FROM: 03/07/2023\
Sum of prices (TO + FROM) -> Set number 2 is: 65.0 (46.15)  ->  TO: 02/07/2023 FROM: 04/07/2023\
Sum of prices (TO + FROM) -> Set number 3 is: 0.0 (0.0)  ->  TO: 03/07/2023 FROM: 05/07/2023\
Sum of prices (TO + FROM) -> Set number 4 is: 51.0 (36.21)  ->  TO: 04/07/2023 FROM: 06/07/2023\
Sum of prices (TO + FROM) -> Set number 5 is: 0.0 (0.0)  ->  TO: 05/07/2023 FROM: 07/07/2023\
Sum of prices (TO + FROM) -> Set number 6 is: 0.0 (0.0)  ->  TO: 06/07/2023 FROM: 08/07/2023\
Sum of prices (TO + FROM) -> Set number 7 is: 54.0 (38.339)  ->  TO: 07/07/2023 FROM: 09/07/2023\
Sum of prices (TO + FROM) -> Set number 8 is: 0.0 (0.0)  ->  TO: 08/07/2023 FROM: 10/07/2023\
Sum of prices (TO + FROM) -> Set number 9 is: 53.0 (37.629)  ->  TO: 09/07/2023 FROM: 11/07/2023\
Sum of prices (TO + FROM) -> Set number 10 is: 0.0 (0.0)  ->  TO: 10/07/2023 FROM: 12/07/2023\
Sum of prices (TO + FROM) -> Set number 11 is: 49.0 (34.79)  ->  TO: 11/07/2023 FROM: 13/07/2023\
Sum of prices (TO + FROM) -> Set number 12 is: 0.0 (0.0)  ->  TO: 12/07/2023 FROM: 14/07/2023\
############################################################################################################\
--------------------->Example of best result:Minimum value: 49.0 (34.79)  Euro  TO: 11/07/2023 FROM: 13/07/2023\
############################################################################################################\
Do you want to try again? [0 = No, 1 = Yes]


Summing up, the algorithm found the lowest price for starting the journey on 11/07/2023 and back on 13/07/2023.
The cost is 49 euros.
The values in brackets show the prices reduced by 29 percent for approximation to the values which are available in the official KIWI app. 