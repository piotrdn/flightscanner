package com.example.flightscanner;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Flights {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) ////IDENTITY inkrementacja o 1, AUTO-doesn't work. With the generation GenerationType.AUTO hibernate will look for the default hibernate_sequence table , so change generation to IDENTITY
    private Long id;

    private String flyFrom;
    private String flyTo;
    //private String cityFrom;
    //private String cityTo;
    private Float price;

    //private String dateFrom;
    //private String dateTo;
    private String localDeparture;



    //for DB save
    public Flights(String flyFrom, String flyTo, Float price, String localDeparture) {
        //this.id = id;
        this.flyFrom = flyFrom;
        this.flyTo = flyTo;
        this.price = price;
        this.localDeparture = localDeparture;
    }



    public Flights() {

    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

//    public String getCityfrom() {
//        return cityFrom;
//    }

//    public void setCityfrom(String cityfrom) {
//        this.cityFrom = cityfrom;
//    }

//    public String getCityTo() {
//        return cityTo;
//    }

//    public void setCityTo(String cityTo) {
//        this.cityTo = cityTo;
//    }

    public Float getPirce() {
        return price;
    }

    public void setPirce(Float pirce) {
        this.price = pirce;
    }

//    public String getCityFrom() {
//        return cityFrom;
//    }

//    public void setCityFrom(String cityFrom) {
//        this.cityFrom = cityFrom;
//    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }


//    public String getDateFrom() {
//        return dateFrom;
//    }

//    public void setDateFrom(String dateFrom) {
//        this.dateFrom = dateFrom;
//    }

//    public String getDateTo() {
//        return dateTo;
//    }

//    public void setDateTo(String dateTo) {
//        this.dateTo = dateTo;
//    }

    public String getLocalDeparture() {
        return localDeparture;
    }

    public void setLocalDeparture(String localDeparture) {
        this.localDeparture = localDeparture;
    }

    @Override
    public String toString() {
        return "Flights{" +
                "flyFrom='" + flyFrom + '\'' +
                ", flyTo='" + flyTo + '\'' +
                //", cityFrom='" + cityFrom + '\'' +
                //", cityTo='" + cityTo + '\'' +
                ", price=" + price +
                '}';
    }
}
