package com.example.flightscanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import java.util.Scanner;

@Component
public class Logic {

    private MethodBase methodBase;

    @Autowired
    public Logic(MethodBase methodBase) {
        this.methodBase = methodBase;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void mainMethod() {

        try{

            System.out.println("######################################################");
            System.out.println("########## Flight Scanner app ##########");

            int var = 1;
            while(var == 1) {


                String dataTo;
                String dataFrom;
                Scanner scanner5 = new Scanner(System.in);
                System.out.println("Put the date TO [DD/MM/YYYY]: ");
                dataTo = scanner5.nextLine();

                Scanner scanner6 = new Scanner(System.in);
                System.out.println("Put the date FROM [DD/MM/YYYY]: ");
                dataFrom = scanner6.nextLine();

                Scanner scanner7 = new Scanner(System.in);
                System.out.println("Fly from: ");
                String flyFrom = scanner7.nextLine();

                Scanner scanner8 = new Scanner(System.in);
                System.out.println("Fly to: ");
                String flyTo = scanner8.nextLine();

                Scanner scanner9 = new Scanner(System.in);
                System.out.println("Holidays number of days: ");
                int daysCount = scanner9.nextInt();
                methodBase.mainAlgorithm(dataTo, dataFrom, daysCount, flyFrom, flyTo);

//                methodBase.mainAlgorithm("01/06/2023", "12/06/2023", 2); //test


                System.out.println("Do you want to try again? [0 = No, 1 = Yes]");
                int temp = methodBase.formProtMethod();
                if(temp == 0){
                    var = 0;
                    System.out.println("Good bye!");
                }
                else
                    var = 1;
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

}