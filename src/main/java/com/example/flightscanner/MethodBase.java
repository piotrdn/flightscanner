package com.example.flightscanner;

import com.example.flightscanner.Repository.FlightScannerRepository;
import com.example.flightscanner.jsonSchema2pojo.Example;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class MethodBase {

    private FlightScannerRepository flightScannerRepository;
    @Autowired
    public MethodBase(FlightScannerRepository flightScannerRepository) {
        this.flightScannerRepository = flightScannerRepository;
    }


    String [] tabToPubl = new String[60];
    String [] tabFromPubl = new String[60];
    String [] tabWithDateTo = new String[60];//????
    String [] tabWithDateFrom = new String[60];//????
    public List<Flights> listFlightsTo = new ArrayList<>(); //w przypadku lotów bezpośrednich do listy trafia maksymalnie jeden element ponieważ jest tylko jeden bezpośredni lot dziennie (RyarAir)
    public List<Flights> listFlightsFrom = new ArrayList<>(); //w przypadku lotów bezpośrednich do listy trafia maksymalnie jeden element ponieważ jest tylko jeden bezpośredni lot dziennie (RyarAir)

    public float [] tabSum = new float[100]; //mainAlgorithm  dla sumy ceny w jedną i druga stronę
    public float[] resultTab;
    public String dateToConv; //mainAlgorithm
    public String dateFromConv; //mainAlgorithm
    public String dataFrompubl;
    public String dataTopubl;
    public float minimumSum = 9999;
    public int index;//for check minimum value of price

    public String prepareUrl() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj datę FROM [DD/MM/YYYY]: ");
        dataFrompubl = scanner.nextLine();

        //Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj datę TO [DD/MM/YYYY]: ");
        dataTopubl = scanner.nextLine();

        //scanner = new Scanner(System.in);
        System.out.println("Fly from: ");
        String flyFrom = scanner.nextLine();

        //Scanner scanner = new Scanner(System.in);
        System.out.println("Fly to: ");
        String flyTo = scanner.nextLine();

        //https://tequila.kiwi.com/portal/docs/tequila_api/search_api   API docs
        String url = "https://api.tequila.kiwi.com/v2/search?fly_from="
                + flyFrom
                +"&fly_to="
                + flyTo
                + "&date_from="
                + dataFrompubl
                + "&date_to="
                + dataTopubl
                + "&flight_type=oneway&ret_from_diff_city=false&ret_to_diff_city=false&adults=1&only_working_days=false&only_weekends=false&curr=EUR&locale=pl&vehicle_type=aircraft&limit=10&max_stopovers=0";

        System.out.println(url);

        return url;
    }

    public String prepareUrlwithParams(String dateFrom, String dateTo, String flyFrom, String flyTo) {

        String url = "https://api.tequila.kiwi.com/v2/search?fly_from="
                + flyFrom
                +"&fly_to="
                + flyTo
                + "&date_from="
                + dateFrom
                + "&date_to="
                + dateTo
                + "&flight_type=oneway&ret_from_diff_city=false&ret_to_diff_city=false&adults=1&only_working_days=false&only_weekends=false&curr=EUR&locale=pl&vehicle_type=aircraft&limit=10&max_stopovers=0";

        System.out.println(url);

        return url;
    }

    public String achieveJson(String url) {

        Client client = new Client();

        WebResource webResource = client.resource(url);
        ClientResponse webResponse = webResource.accept("application/json")
                .header("apikey", "710z8Snz2h63eStd40G7sUT6UbdRzdxX")
                .get(ClientResponse.class);

        if (webResponse.getStatus() != 200) {
            throw new RuntimeException("HTTP Error ..." + webResponse.getStatus());
        }

        String json = webResponse.getEntity(String.class);
        System.out.println(json);

        return json;
    }

    public void forDBsave(String json) throws JsonProcessingException {

        //uzycie Jacksona
        ObjectMapper mapper = new ObjectMapper();
        Example example = mapper.readValue(json, Example.class);

        for(int i=0;i<example.getData().size();i++) {
            String flyFrom = example.getData().get(i).getFlyFrom();
            String flyTo = example.getData().get(i).getFlyTo();
            //String cityFrom = example.getData().get(i).getCityFrom();
           //String cityTo = example.getData().get(i).getCityTo();
            Float price = Float.valueOf(example.getData().get(i).getPrice());
            String localDeparture = example.getData().get(i).getLocalDeparture();

            Long id = 0l;
            Flights flights = new Flights(flyFrom, flyTo, price, localDeparture);
            flightScannerRepository.save(flights);
        }

        System.out.println("Saved to the DB!");

    }

    public int formProtMethod() {

        String userInput;
        int var0 = 1;
        int temp3 = 1;
        Scanner scannerResp = new Scanner(System.in);

        while(var0 == 1) {                                                              //format protection
            userInput = scannerResp.next(); //scan the input
            try {
                temp3 = Integer.parseInt(userInput); // try to parse the "number" to int

                if (temp3 != 0 && temp3 != 1) {
                    System.out.println("Wrong value. Please type 1 or 0.");
                }
                else {
                    var0 = 0;
                }
            } catch (NumberFormatException e) {
                System.out.println("Hey, you entered something wrong... You should type a number!");
            }
        }

        return temp3;

    }

    public List readJson(String json) throws JsonProcessingException {
        List<Flights> flightsList = new ArrayList<>();
        //uzycie Jacksona
        ObjectMapper mapper = new ObjectMapper();
        Example example = mapper.readValue(json, Example.class);


        for(int i=0;i<=example.getData().size()-1;i++) {
            Flights flights = new Flights();
            flights.setFlyFrom(example.getData().get(i).getFlyFrom());
            flights.setFlyTo(example.getData().get(i).getFlyTo());
            //flights.setCityFrom(example.getData().get(i).getCityFrom());
            //flights.setCityTo(example.getData().get(i).getCityTo());
            flights.setPirce(Float.valueOf(example.getData().get(i).getPrice()));
            flights.setLocalDeparture(example.getData().get(i).getLocalDeparture());

            flightsList.add(flights);
            //System.out.println(flightsList.get(i));
        }

        return flightsList;
    }


    public long timeConversion(String time) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH); //Specify your locale
        long unixTime = 0;
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+1:00")); //Specify your timezone
        try {
            unixTime = dateFormat.parse(time).getTime();
            unixTime = unixTime / 1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return unixTime;
    }

    public String converterMethod(int unixtimestamp) {

        Date date = new Date(unixtimestamp*1000L);
        SimpleDateFormat jdf = new SimpleDateFormat("dd/MM/yyyy");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
        String java_date = jdf.format(date);
        return java_date;
    }

    public void mainAlgorithm(String dateTo, String dateFrom, int daysCount, String flyFrom, String flyTo) {

        //wyliczenie zakresu przeszukiwań:
        Long dateFromTimestamp = timeConversion(dateFrom);
        Long dateToTimestamp = timeConversion(dateTo);
        Long numberOfDays = ((dateFromTimestamp - dateToTimestamp) / 86400)+1;
        System.out.println("Number of days: " + numberOfDays);

        for(int i=1;i<=(numberOfDays-daysCount);i++) { //iteracja po każdej możliwej parze (z uwględnieniem czasu trwania urlopu)
            System.out.println();
            System.out.println("Option number: " + i);

            //pobranie ceny lotu DO z dnia 01
            //String urlTo = prepareUrlwithParams(i + "/06/2023", i+"/06/2023", "GDN", "WRO");//test
            String unitTimestampDateTo = String.valueOf(timeConversion(dateTo));
            dateToConv = converterMethod(Integer.valueOf(unitTimestampDateTo)+(i-1)*86400);
            String urlTo = prepareUrlwithParams(dateToConv, dateToConv, flyFrom, flyTo);//podawane są te same daty bo interesuje nas ten jeden konkretny dzień
            String jsonTo = achieveJson(urlTo);
            try{
                listFlightsTo = readJson(jsonTo);

                if(listFlightsTo.size() == 0) {
                    System.out.println("There is lack of flights for this day!");
                }else{
                    System.out.println("Price "
                            + flyFrom
                            + " -> "
                            + flyTo
                            + " "
                            + listFlightsTo.get(0).getPirce()
                            + " ("
                            + (listFlightsTo.get(0).getPirce())*0.71 //value *0.71 is for fit price to the KIWI.com price
                            + ")" + " "
                            + listFlightsTo.get(0).getLocalDeparture()); //get(0) - założenie: jeden lot dziennie. W przypadku RyanAir dla lotow bezpośrednich tak faktycznie jest

                    tabWithDateTo[i] = listFlightsTo.get(0).getLocalDeparture();//additional
                    //DB save
                    forDBsave(jsonTo);
                }
                System.out.println("Date TO: " + dateToConv);
                tabToPubl[i] = dateToConv;

            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

            //pobranie ceny lotu POWROTNEGO z dnia 01 + daysCount
            //String urlFrom = prepareUrlwithParams(i+daysCount+"/06/2023", i+daysCount+"/06/2023", "WRO", "GDN");//test
            dateFromConv = converterMethod(Integer.valueOf(unitTimestampDateTo)+(daysCount + i-1)*86400);
            String urlFrom = prepareUrlwithParams(dateFromConv, dateFromConv, flyTo, flyFrom);
            String jsonFrom = achieveJson(urlFrom);
            try{
                listFlightsFrom = readJson(jsonFrom);

                if(listFlightsFrom.size() == 0){
                    System.out.println("There is lack of flights for this day!");
                }
                else {
                    System.out.println("Price "
                            + flyTo
                            + " -> "
                            + flyFrom
                            + " "
                            + listFlightsFrom.get(0).getPirce()
                            + " ("
                            + (listFlightsFrom.get(0).getPirce())*0.71 //value *0.71 is for fit price to the KIWI.com price
                            + ")"
                            + " "
                            + listFlightsFrom.get(0).getLocalDeparture()); //get(0) - założenie: jeden lot dziennie. W przypadku RyanAir dla lotow bezpośrednich tak faktycznie jest

                    tabWithDateFrom[i] = listFlightsFrom.get(0).getLocalDeparture();//additional
                    //DB save
                    forDBsave(jsonFrom);
                }
                System.out.println("Date FROM: " + dateFromConv);
                tabFromPubl[i] = dateFromConv;


            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

            //SUM of available flights
            if((listFlightsTo.size() != 0) && (listFlightsFrom.size() != 0)) {
                tabSum[i] = listFlightsTo.get(0).getPirce() + listFlightsFrom.get(0).getPirce();
            }
            System.out.println("Sum "+ i + ": " + tabSum[i]);

        }

        System.out.println();
        System.out.println("Results: ");

        System.out.println();
        for(int j=1;j<=(numberOfDays-daysCount);j++) { //sum up all options
            System.out.println("Sum of prices (TO + FROM) -> Set number "
                    + j
                    + " is: "
                    + tabSum[j]
                    + " ("
                    + (tabSum[j]*0.71) //value *0.71 is for fit price to the KIWI.com price
                    + ") " + " -> " + " TO: "
                    + tabToPubl[j]
                    + " FROM: "
                    + tabFromPubl[j]);

            //TEST
            //System.out.println("NEW-> Sum of prices (TO + FROM) -> Set number " + j + " is: " + tabSum[j] + " (" + (tabSum[j]*0.71) + ") " + " -> " + " TO: " + tabToPubl[j] + " FROM: " + tabFromPubl[j] + "dateTo: " + tabWithDateTo[j] + "Date FROM: " + tabWithDateFrom[j]);


            //get the lowest price
            if(tabSum[j] != 0 ) {

                if (tabSum[j] <= minimumSum) {
                    minimumSum = tabSum[j];
                    //minimumSumTab[j] = tabSum[j]; //do zapisu kilku najniższych cen //bez sensu
                }
//                if (tabSum[j] == minimumSum) {
//                    minimumSumTab[j] = tabSum[j]; //do zapisu kilku najniższych cen
//                }

                if(tabSum[j] == minimumSum) {
                    index = j;
                }
            }
        }
        System.out.println("##########################################################################################################################");
        System.out.println("--------------------->Example of best result:" + "Minimum value: " + minimumSum + " (" + (minimumSum*0.71) + ") " + " Euro" + " " + " TO: " + tabToPubl[index] + " FROM: " + tabFromPubl[index]);
        System.out.println("##########################################################################################################################");



//        int length = getMinIndexes(tabSum).length;
//        //List<float[]> listForTab = new ArrayList<>(Arrays.asList(sumTab2));
//        listForTab = new ArrayList<>(Arrays.asList(sumTab2));
//        System.out.println();
//        System.out.println("Indeksy zawierajaca najniższą cenę");
//        for(int i=0;i<length;i++) {
//            System.out.println(getMinIndexes(tabSum)[i]);
//        }
//        System.out.println("listForTab size: " + listForTab.size()); //1
//        System.out.println("sumTab2 length: " + sumTab2.length); //97
//        System.out.println("length: " + length); //97
//        System.out.println("tabSum length: " + tabSum.length); //100
//        System.out.println("resultTab length: " + resultTab.length); //
//
//        for(int i =0;i<= listForTab.size();i++) {
//            System.out.println("##########################################################################################################################");
//            System.out.println("---------------------> Best date:" + "Minimum value: " + minimumSum + " (" + (minimumSum*0.71) + ") " + " Euro" + " " + " TO: " + tabToPubl[i] + " FROM: " + tabFromPubl[i] + " -> " + "TO: " + tabWithDateTo[i] + "FROM: " + tabWithDateFrom[i]);
//            System.out.println("##########################################################################################################################");
//        }

    }


//    public float[] getMinIndexes(float[] sumTab) {
//        float[] result = new float[sumTab.length];
//        float min = Integer.MAX_VALUE;
//        float count = 0;
//
//        // znajdź najmniejszą wartość w tablicy
//        for (int i = 0; i < sumTab.length; i++) {
//            if (sumTab[i] < min) {
//                min = sumTab[i];
//            }
//        }
//
//        // dodaj indeksy elementów o najmniejszej wartości do tablicy wynikowej
//        for (int i = 0; i < sumTab.length; i++) {
//            if (sumTab[i] == min) {
//                result[(int) count++] = i;
//            }
//        }
//
//        // przyciąć tablicę wynikową do rozmiaru faktycznego
//        return Arrays.copyOf(result, (int) count);
//
//    }

}