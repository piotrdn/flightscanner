package com.example.flightscanner.Repository;

import com.example.flightscanner.Flights;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightScannerRepository extends JpaRepository<Flights, Long> {
}
