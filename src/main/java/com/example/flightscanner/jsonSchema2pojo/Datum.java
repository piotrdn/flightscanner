package com.example.flightscanner.jsonSchema2pojo;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
//import javax.annotation.Generated;
import javax.annotation.processing.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "flyFrom",
        "flyTo",
        "cityFrom",
        "cityCodeFrom",
        "cityTo",
        "cityCodeTo",
        "countryFrom",
        "countryTo",
        "nightsInDest",
        "quality",
        "distance",
        "duration",
        "price",
        "conversion",
        "bags_price",
        "baglimit",
        "availability",
        "airlines",
        "route",
        "booking_token",
        "facilitated_booking_available",
        "pnr_count",
        "has_airport_change",
        "technical_stops",
        "throw_away_ticketing",
        "hidden_city_ticketing",
        "virtual_interlining",
        "local_arrival",
        "utc_arrival",
        "local_departure",
        "utc_departure"
})
@Generated("jsonschema2pojo")
public class Datum {

    @JsonProperty("id")
    private String id;
    @JsonProperty("flyFrom")
    private String flyFrom;
    @JsonProperty("flyTo")
    private String flyTo;
    @JsonProperty("cityFrom")
    private String cityFrom;
    @JsonProperty("cityCodeFrom")
    private String cityCodeFrom;
    @JsonProperty("cityTo")
    private String cityTo;
    @JsonProperty("cityCodeTo")
    private String cityCodeTo;
    @JsonProperty("countryFrom")
    private CountryFrom countryFrom;
    @JsonProperty("countryTo")
    private CountryTo countryTo;
    @JsonProperty("nightsInDest")
    private Object nightsInDest;
    @JsonProperty("quality")
    private Double quality;
    @JsonProperty("distance")
    private Double distance;
//    @JsonProperty("duration")
//    private Duration duration;
    @JsonProperty("price")
    private Integer price;
//    @JsonProperty("conversion")
//    private Conversion conversion;
//    @JsonProperty("bags_price")
//    private BagsPrice bagsPrice;
//    @JsonProperty("baglimit")
//    private Baglimit baglimit;
//    @JsonProperty("availability")
//    private Availability availability;
    @JsonProperty("airlines")
    private List<String> airlines;
//    @JsonProperty("route")
//    private List<Route> route;
    @JsonProperty("booking_token")
    private String bookingToken;
    @JsonProperty("facilitated_booking_available")
    private Boolean facilitatedBookingAvailable;
    @JsonProperty("pnr_count")
    private Integer pnrCount;
    @JsonProperty("has_airport_change")
    private Boolean hasAirportChange;
    @JsonProperty("technical_stops")
    private Integer technicalStops;
    @JsonProperty("throw_away_ticketing")
    private Boolean throwAwayTicketing;
    @JsonProperty("hidden_city_ticketing")
    private Boolean hiddenCityTicketing;
    @JsonProperty("virtual_interlining")
    private Boolean virtualInterlining;
    @JsonProperty("local_arrival")
    private String localArrival;
    @JsonProperty("utc_arrival")
    private String utcArrival;
    @JsonProperty("local_departure")
    private String localDeparture;
    @JsonProperty("utc_departure")
    private String utcDeparture;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("flyFrom")
    public String getFlyFrom() {
        return flyFrom;
    }

    @JsonProperty("flyFrom")
    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    @JsonProperty("flyTo")
    public String getFlyTo() {
        return flyTo;
    }

    @JsonProperty("flyTo")
    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    @JsonProperty("cityFrom")
    public String getCityFrom() {
        return cityFrom;
    }

    @JsonProperty("cityFrom")
    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    @JsonProperty("cityCodeFrom")
    public String getCityCodeFrom() {
        return cityCodeFrom;
    }

    @JsonProperty("cityCodeFrom")
    public void setCityCodeFrom(String cityCodeFrom) {
        this.cityCodeFrom = cityCodeFrom;
    }

    @JsonProperty("cityTo")
    public String getCityTo() {
        return cityTo;
    }

    @JsonProperty("cityTo")
    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    @JsonProperty("cityCodeTo")
    public String getCityCodeTo() {
        return cityCodeTo;
    }

    @JsonProperty("cityCodeTo")
    public void setCityCodeTo(String cityCodeTo) {
        this.cityCodeTo = cityCodeTo;
    }

    @JsonProperty("countryFrom")
    public CountryFrom getCountryFrom() {
        return countryFrom;
    }

    @JsonProperty("countryFrom")
    public void setCountryFrom(CountryFrom countryFrom) {
        this.countryFrom = countryFrom;
    }

    @JsonProperty("countryTo")
    public CountryTo getCountryTo() {
        return countryTo;
    }

    @JsonProperty("countryTo")
    public void setCountryTo(CountryTo countryTo) {
        this.countryTo = countryTo;
    }

    @JsonProperty("nightsInDest")
    public Object getNightsInDest() {
        return nightsInDest;
    }

    @JsonProperty("nightsInDest")
    public void setNightsInDest(Object nightsInDest) {
        this.nightsInDest = nightsInDest;
    }

    @JsonProperty("quality")
    public Double getQuality() {
        return quality;
    }

    @JsonProperty("quality")
    public void setQuality(Double quality) {
        this.quality = quality;
    }

    @JsonProperty("distance")
    public Double getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(Double distance) {
        this.distance = distance;
    }

//    @JsonProperty("duration")
//    public Duration getDuration() {
//        return duration;
//    }
//
//    @JsonProperty("duration")
//    public void setDuration(Duration duration) {
//        this.duration = duration;
//    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

//    @JsonProperty("conversion")
//    public Conversion getConversion() {
//        return conversion;
//    }
//
//    @JsonProperty("conversion")
//    public void setConversion(Conversion conversion) {
//        this.conversion = conversion;
//    }
//
//    @JsonProperty("bags_price")
//    public BagsPrice getBagsPrice() {
//        return bagsPrice;
//    }
//
//    @JsonProperty("bags_price")
//    public void setBagsPrice(BagsPrice bagsPrice) {
//        this.bagsPrice = bagsPrice;
//    }
//
//    @JsonProperty("baglimit")
//    public Baglimit getBaglimit() {
//        return baglimit;
//    }
//
//    @JsonProperty("baglimit")
//    public void setBaglimit(Baglimit baglimit) {
//        this.baglimit = baglimit;
//    }
//
//    @JsonProperty("availability")
//    public Availability getAvailability() {
//        return availability;
//    }
//
//    @JsonProperty("availability")
//    public void setAvailability(Availability availability) {
//        this.availability = availability;
//    }

    @JsonProperty("airlines")
    public List<String> getAirlines() {
        return airlines;
    }

    @JsonProperty("airlines")
    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

//    @JsonProperty("route")
//    public List<Route> getRoute() {
//        return route;
//    }
//
//    @JsonProperty("route")
//    public void setRoute(List<Route> route) {
//        this.route = route;
//    }

    @JsonProperty("booking_token")
    public String getBookingToken() {
        return bookingToken;
    }

    @JsonProperty("booking_token")
    public void setBookingToken(String bookingToken) {
        this.bookingToken = bookingToken;
    }

    @JsonProperty("facilitated_booking_available")
    public Boolean getFacilitatedBookingAvailable() {
        return facilitatedBookingAvailable;
    }

    @JsonProperty("facilitated_booking_available")
    public void setFacilitatedBookingAvailable(Boolean facilitatedBookingAvailable) {
        this.facilitatedBookingAvailable = facilitatedBookingAvailable;
    }

    @JsonProperty("pnr_count")
    public Integer getPnrCount() {
        return pnrCount;
    }

    @JsonProperty("pnr_count")
    public void setPnrCount(Integer pnrCount) {
        this.pnrCount = pnrCount;
    }

    @JsonProperty("has_airport_change")
    public Boolean getHasAirportChange() {
        return hasAirportChange;
    }

    @JsonProperty("has_airport_change")
    public void setHasAirportChange(Boolean hasAirportChange) {
        this.hasAirportChange = hasAirportChange;
    }

    @JsonProperty("technical_stops")
    public Integer getTechnicalStops() {
        return technicalStops;
    }

    @JsonProperty("technical_stops")
    public void setTechnicalStops(Integer technicalStops) {
        this.technicalStops = technicalStops;
    }

    @JsonProperty("throw_away_ticketing")
    public Boolean getThrowAwayTicketing() {
        return throwAwayTicketing;
    }

    @JsonProperty("throw_away_ticketing")
    public void setThrowAwayTicketing(Boolean throwAwayTicketing) {
        this.throwAwayTicketing = throwAwayTicketing;
    }

    @JsonProperty("hidden_city_ticketing")
    public Boolean getHiddenCityTicketing() {
        return hiddenCityTicketing;
    }

    @JsonProperty("hidden_city_ticketing")
    public void setHiddenCityTicketing(Boolean hiddenCityTicketing) {
        this.hiddenCityTicketing = hiddenCityTicketing;
    }

    @JsonProperty("virtual_interlining")
    public Boolean getVirtualInterlining() {
        return virtualInterlining;
    }

    @JsonProperty("virtual_interlining")
    public void setVirtualInterlining(Boolean virtualInterlining) {
        this.virtualInterlining = virtualInterlining;
    }

    @JsonProperty("local_arrival")
    public String getLocalArrival() {
        return localArrival;
    }

    @JsonProperty("local_arrival")
    public void setLocalArrival(String localArrival) {
        this.localArrival = localArrival;
    }

    @JsonProperty("utc_arrival")
    public String getUtcArrival() {
        return utcArrival;
    }

    @JsonProperty("utc_arrival")
    public void setUtcArrival(String utcArrival) {
        this.utcArrival = utcArrival;
    }

    @JsonProperty("local_departure")
    public String getLocalDeparture() {
        return localDeparture;
    }

    @JsonProperty("local_departure")
    public void setLocalDeparture(String localDeparture) {
        this.localDeparture = localDeparture;
    }

    @JsonProperty("utc_departure")
    public String getUtcDeparture() {
        return utcDeparture;
    }

    @JsonProperty("utc_departure")
    public void setUtcDeparture(String utcDeparture) {
        this.utcDeparture = utcDeparture;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

