package com.example.flightscanner.jsonSchema2pojo;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
//import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "search_id",
        "currency",
        "fx_rate",
        "data",
        "_results",
        "search_params",
        "all_stopover_airports",
        "sort_version"
})
@Generated("jsonschema2pojo")
public class Example {

    @JsonProperty("search_id")
    private String searchId;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("fx_rate")
    private Integer fxRate;
    @JsonProperty("data")
    private List<Datum> data;
    @JsonProperty("_results")
    private Integer results;
//    @JsonProperty("search_params")
//    private SearchParams searchParams;
    @JsonProperty("all_stopover_airports")
    private List<Object> allStopoverAirports;
    @JsonProperty("sort_version")
    private Integer sortVersion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    @JsonProperty("search_id")
    public String getSearchId() {
        return searchId;
    }

    @JsonProperty("search_id")
    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("fx_rate")
    public Integer getFxRate() {
        return fxRate;
    }

    @JsonProperty("fx_rate")
    public void setFxRate(Integer fxRate) {
        this.fxRate = fxRate;
    }

    @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonProperty("_results")
    public Integer getResults() {
        return results;
    }

    @JsonProperty("_results")
    public void setResults(Integer results) {
        this.results = results;
    }

//    @JsonProperty("search_params")
//    public SearchParams getSearchParams() {
//        return searchParams;
//    }
//
//    @JsonProperty("search_params")
//    public void setSearchParams(SearchParams searchParams) {
//        this.searchParams = searchParams;
//    }

    @JsonProperty("all_stopover_airports")
    public List<Object> getAllStopoverAirports() {
        return allStopoverAirports;
    }

    @JsonProperty("all_stopover_airports")
    public void setAllStopoverAirports(List<Object> allStopoverAirports) {
        this.allStopoverAirports = allStopoverAirports;
    }

    @JsonProperty("sort_version")
    public Integer getSortVersion() {
        return sortVersion;
    }

    @JsonProperty("sort_version")
    public void setSortVersion(Integer sortVersion) {
        this.sortVersion = sortVersion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}