package com.example.flightscanner;

import com.example.flightscanner.Repository.FlightScannerRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


//@SpringBootTest
@RunWith(JUnit4.class)//it allows not to comment (switch off) the EventListener! :)
class FlightscannerApplicationTests {

	private MethodBase methodBase;
	private FlightScannerRepository flightScannerRepository;

	@BeforeEach
	public void beforEach() {
		methodBase = new MethodBase(flightScannerRepository);
	}


	@Test
	void prepareUrlwithParams_test() {
		//given
		String dateFrom = "01/06/2023"; //date_to and date_from is the same because we need to get only results for this specific day
		String dateTo = "01/06/2023";
		String flyFrom = "GDN";
		String flyTo = "WRO";

		//when
		String actualResult = methodBase.prepareUrlwithParams(dateFrom, dateTo, flyFrom, flyTo);
		String equalTo = "https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO&date_from=01/06/2023&date_to=01/06/2023&flight_type=oneway&ret_from_diff_city=false&ret_to_diff_city=false&adults=1&only_working_days=false&only_weekends=false&curr=EUR&locale=pl&vehicle_type=aircraft&limit=10&max_stopovers=0";

		//then
		Assertions.assertThat(actualResult).contains(flyFrom, flyTo, "fly_from="+flyFrom+"&fly_to="+flyTo+"&date_from="+dateFrom+"&date_to="+dateTo);
		Assertions.assertThat(actualResult).isEqualTo(equalTo);
		org.junit.jupiter.api.Assertions.assertTrue(actualResult.contains("fly_from="+flyFrom));
		org.junit.jupiter.api.Assertions.assertTrue(actualResult.contains("fly_to="+flyTo));
		org.junit.jupiter.api.Assertions.assertTrue(actualResult.contains("date_from="+dateFrom));
		org.junit.jupiter.api.Assertions.assertTrue(actualResult.contains("date_to="+dateTo));

	}

	@Test
	void prepareUrl_test() { //in method in methodBase class we have to definied only one scanner
		//given
		//String input = "01/06/2023\n" + "14/06/2023\n" + "GDN\n" + "WRO\n";
		String flyTo = "WRO";
		String flyFrom = "GDN";
		String dateTo = "01/06/2023";
		String dateFrom = "01/06/2023"; //date_to and date_from is the same because we need to get only results for this specific day
		String input2 = dateFrom +"\n" + dateTo + "\n" + flyFrom + "\n" + flyTo + "\n";

		String urlResult;
		InputStream in = new ByteArrayInputStream(input2.getBytes());
		System.setIn(in);

		//when
		urlResult = methodBase.prepareUrl();

		//then
		Assertions.assertThat(urlResult).containsOnlyOnce("fly_from="+flyFrom+"&fly_to="+flyTo+"&date_from="+dateFrom+"&date_to="+dateTo);
		org.junit.jupiter.api.Assertions.assertTrue(urlResult.contains("fly_from="+flyFrom));
		org.junit.jupiter.api.Assertions.assertTrue(urlResult.contains("fly_to="+flyTo));
		org.junit.jupiter.api.Assertions.assertTrue(urlResult.contains("date_from="+dateFrom));
		org.junit.jupiter.api.Assertions.assertTrue(urlResult.contains("date_to="+dateTo));

	}

	@Test
	public void achieveJson_test_positive() { //date_to and date_from is the same because we need to get only results for this specific day
		//given
		String url = "https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO&date_from=01/06/2023&date_to=01/06/2023&flight_type=oneway&ret_from_diff_city=false&ret_to_diff_city=false&adults=1&only_working_days=false&only_weekends=false&curr=EUR&locale=pl&vehicle_type=aircraft&limit=5&max_stopovers=0";
		//when
		String jsonResult = methodBase.achieveJson(url);

		//then
		Assertions.assertThat(jsonResult).contains("\"flyFrom\": \"GDN\"");
		Assertions.assertThat(jsonResult).contains("\"flyTo\": \"WRO\"");
		Assertions.assertThat(jsonResult).contains("\"cityCodeFrom\": \"GDN\"");
		Assertions.assertThat(jsonResult).contains("\"cityCodeTo\": \"WRO\"");
	}
	@Test
	public void achieveJson_test_negative() {
		//given
		String url = "https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO&date_from=14/06/2023&date_to=01/06/2023&flight_type=oneway&ret_from_diff_city=false&ret_to_diff_city=false&adults=1&only_working_days=false&only_weekends=false&curr=EUR&locale=pl&vehicle_type=aircraft&limit=10&max_stopovers=0";

		//when
		// Sprawdzenie, czy metoda rzuca wyjątek RuntimeException dla błędnego kodu HTTP
		assertThrows(RuntimeException.class, () -> {
			methodBase.achieveJson(url);
		});
		// Sprawdzenie, czy wyjątek ma odpowiednią treść
		Exception exception = assertThrows(RuntimeException.class, () -> {
			methodBase.achieveJson(url);
		});

		//then
		String expectedMessage = "HTTP Error ...";
		String actualMessage = exception.getMessage();
		//assertEquals(expectedMessage, actualMessage); //nie zwróci identycznej odpowiedzi ponieważ będzie brakować kodu błędu po ... np 404
		Assertions.assertThat(actualMessage).contains(expectedMessage);
	}

	@Test
	public void formProtMethod_test_positive() {

		for(int i = 0;i<=1;i++){
			//given
			String input = String.valueOf(i);
			InputStream in = new ByteArrayInputStream(input.getBytes());
			System.setIn(in);

			int expectedValue = i;

			//when
			int result = methodBase.formProtMethod();

			//then
			Assertions.assertThat(result).isEqualTo(expectedValue);
		}
	}

	@Test
	public void formProtMethod_test_negative() {
		//given
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); //method returns error message using System.out.println()...
		System.setOut(new PrintStream(outputStream));

		String errorMessage = "Hey, you entered something wrong... You should type a number!";
		String input = "r";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		//when
		// Sprawdzenie, czy metoda rzuca wyjątek NoSuchElementException
		assertThrows(NoSuchElementException.class, () -> {
			methodBase.formProtMethod();
		});

		// Sprawdzenie, czy wyjątek ma odpowiednią treść (w ten sposób się nie da ponieważ wyjątek zwraca swój opis poprzez System.out.println...)
//		Exception exception = assertThrows(NoSuchElementException.class, () -> {
//			methodBase.formProtMethod();
//		});

		//then
		String consoleOutput = outputStream.toString().trim();
		Assertions.assertThat(errorMessage).isEqualTo(consoleOutput);

		assertEquals(errorMessage, consoleOutput);
		System.setOut(System.out); //Ustawienie standardowego wyjścia za pomocą System.setOut(System.out), aby nie wpłynęło to na inne testy

	}
	@Test
	public void formProtMethod_test_negative2() {
		//given
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); //method returns error message using System.out.println()...
		System.setOut(new PrintStream(outputStream));

		String errorMessage = "Wrong value. Please type 1 or 0.";
		String input = "2";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		//when
		// Sprawdzenie, czy metoda rzuca wyjątek NoSuchElementException
		assertThrows(NoSuchElementException.class, () -> {
			methodBase.formProtMethod();
		});

		// Sprawdzenie, czy wyjątek ma odpowiednią treść (w ten sposób się nie da ponieważ wyjątek zwraca swój opis poprzez System.out.println...)
//		Exception exception = assertThrows(NoSuchElementException.class, () -> {
//			methodBase.formProtMethod();
//		});

		//then
		String consoleOutput = outputStream.toString().trim();
		Assertions.assertThat(errorMessage).isEqualTo(consoleOutput);

		assertEquals(errorMessage, consoleOutput);
		System.setOut(System.out); //Ustawienie standardowego wyjścia za pomocą System.setOut(System.out), aby nie wpłynęło to na inne testy

	}
	@Test
	public void readJson_test() throws JsonProcessingException {
		//given
		List<Flights> resultList = new ArrayList<>();
		String json = "{\n" +
				"\t\"search_id\": \"755cd188-4002-7b51-3da4-6b8271ace0ab\",\n" +
				"\t\"currency\": \"EUR\",\n" +
				"\t\"fx_rate\": 1,\n" +
				"\t\"data\": [\n" +
				"\t\t{\n" +
				"\t\t\t\"id\": \"078a1ad94c350000c59c1a68_0\",\n" +
				"\t\t\t\"flyFrom\": \"GDN\",\n" +
				"\t\t\t\"flyTo\": \"WRO\",\n" +
				"\t\t\t\"cityFrom\": \"Gdańsk\",\n" +
				"\t\t\t\"cityCodeFrom\": \"GDN\",\n" +
				"\t\t\t\"cityTo\": \"Wrocław\",\n" +
				"\t\t\t\"cityCodeTo\": \"WRO\",\n" +
				"\t\t\t\"countryFrom\": {\n" +
				"\t\t\t\t\"code\": \"PL\",\n" +
				"\t\t\t\t\"name\": \"Polska\"\n" +
				"\t\t\t},\n" +
				"\t\t\t\"countryTo\": {\n" +
				"\t\t\t\t\"code\": \"PL\",\n" +
				"\t\t\t\t\"name\": \"Polska\"\n" +
				"\t\t\t},\n" +
				"\t\t\t\"nightsInDest\": null,\n" +
				"\t\t\t\"quality\": 41.666615,\n" +
				"\t\t\t\"distance\": 379.75,\n" +
				"\t\t\t\"duration\": {\n" +
				"\t\t\t\t\"departure\": 3900,\n" +
				"\t\t\t\t\"return\": 0,\n" +
				"\t\t\t\t\"total\": 3900\n" +
				"\t\t\t},\n" +
				"\t\t\t\"price\": 21,\n" +
				"\t\t\t\"conversion\": {\n" +
				"\t\t\t\t\"EUR\": 21\n" +
				"\t\t\t},\n" +
				"\t\t\t\"fare\": {\n" +
				"\t\t\t\t\"adults\": 21,\n" +
				"\t\t\t\t\"children\": 21,\n" +
				"\t\t\t\t\"infants\": 21\n" +
				"\t\t\t},\n" +
				"\t\t\t\"price_dropdown\": {\n" +
				"\t\t\t\t\"base_fare\": 21,\n" +
				"\t\t\t\t\"fees\": 0\n" +
				"\t\t\t},\n" +
				"\t\t\t\"bags_price\": {\n" +
				"\t\t\t\t\"1\": 32.355,\n" +
				"\t\t\t\t\"2\": 64.725\n" +
				"\t\t\t},\n" +
				"\t\t\t\"baglimit\": {\n" +
				"\t\t\t\t\"hand_height\": 40,\n" +
				"\t\t\t\t\"hand_length\": 55,\n" +
				"\t\t\t\t\"hand_weight\": 10,\n" +
				"\t\t\t\t\"hand_width\": 20,\n" +
				"\t\t\t\t\"hold_dimensions_sum\": 319,\n" +
				"\t\t\t\t\"hold_height\": 119,\n" +
				"\t\t\t\t\"hold_length\": 119,\n" +
				"\t\t\t\t\"hold_weight\": 20,\n" +
				"\t\t\t\t\"hold_width\": 81,\n" +
				"\t\t\t\t\"personal_item_height\": 25,\n" +
				"\t\t\t\t\"personal_item_length\": 40,\n" +
				"\t\t\t\t\"personal_item_weight\": 10,\n" +
				"\t\t\t\t\"personal_item_width\": 20\n" +
				"\t\t\t},\n" +
				"\t\t\t\"availability\": {\n" +
				"\t\t\t\t\"seats\": null\n" +
				"\t\t\t},\n" +
				"\t\t\t\"airlines\": [\n" +
				"\t\t\t\t\"FR\"\n" +
				"\t\t\t],\n" +
				"\t\t\t\"route\": [\n" +
				"\t\t\t\t{\n" +
				"\t\t\t\t\t\"id\": \"078a1ad94c350000c59c1a68_0\",\n" +
				"\t\t\t\t\t\"combination_id\": \"078a1ad94c350000c59c1a68\",\n" +
				"\t\t\t\t\t\"flyFrom\": \"GDN\",\n" +
				"\t\t\t\t\t\"flyTo\": \"WRO\",\n" +
				"\t\t\t\t\t\"cityFrom\": \"Gdańsk\",\n" +
				"\t\t\t\t\t\"cityCodeFrom\": \"GDN\",\n" +
				"\t\t\t\t\t\"cityTo\": \"Wrocław\",\n" +
				"\t\t\t\t\t\"cityCodeTo\": \"WRO\",\n" +
				"\t\t\t\t\t\"airline\": \"FR\",\n" +
				"\t\t\t\t\t\"flight_no\": 4106,\n" +
				"\t\t\t\t\t\"operating_carrier\": \"\",\n" +
				"\t\t\t\t\t\"operating_flight_no\": \"\",\n" +
				"\t\t\t\t\t\"fare_basis\": \"\",\n" +
				"\t\t\t\t\t\"fare_category\": \"M\",\n" +
				"\t\t\t\t\t\"fare_classes\": \"\",\n" +
				"\t\t\t\t\t\"fare_family\": \"\",\n" +
				"\t\t\t\t\t\"return\": 0,\n" +
				"\t\t\t\t\t\"bags_recheck_required\": false,\n" +
				"\t\t\t\t\t\"vi_connection\": false,\n" +
				"\t\t\t\t\t\"guarantee\": false,\n" +
				"\t\t\t\t\t\"equipment\": null,\n" +
				"\t\t\t\t\t\"vehicle_type\": \"aircraft\",\n" +
				"\t\t\t\t\t\"local_arrival\": \"2023-06-01T08:20:00.000Z\",\n" +
				"\t\t\t\t\t\"utc_arrival\": \"2023-06-01T06:20:00.000Z\",\n" +
				"\t\t\t\t\t\"local_departure\": \"2023-06-01T07:15:00.000Z\",\n" +
				"\t\t\t\t\t\"utc_departure\": \"2023-06-01T05:15:00.000Z\"\n" +
				"\t\t\t\t}\n" +
				"\t\t\t],\n" +
				"\t\t\t\"booking_token\": \"FaRS4geriM0XQsY8YerqzWNKs9a4mXb_tWZG1ZhghkXuK9r-UtJffoDzY_96SLwISnPpKfyqYRbSv-xuXpvCnXClCkdZPy9xpdgkPB_Ie1jNvN303zaKVFiKXPYPYTRNn2tY-VmtJfTSjVpd1v1emP7rigKEkIE-pzuFEHozFONd8c4Ysd3REeQx9HQ-uYpvRXT49rZrK3uOuwrLzM9dna92aFI3UvFbH29aDZBIAN9liPTM-OEV50z5hZPhvEKTPmuAGYYSsHyaguhcpMhp5315lSaQ1nGWYzElrbyX_tEEwt7Ep4dbZM-5TTgbduI5sVDoFLNQs7fbvvH5bUeF0ks4MKpd0kStd6KY22UTxLVj_qlMCHK9NLq0VtmavlPHgVPU4Qfs3seijIkVNxEkDXRwEYZjWO0uTu2xNXiM9NnbQ-EytLSbPzu7TvsIhFpy2b8T4wIk0nQzSWxEvbgJCJnrIBrS4NfoflmpD7nnVhmOy3FPufZYTF8eKMQacUbg5z5btsDNQKRB43iS-HGt5ZCsQ0XVs-q5Fl304Na_AhkBIaNAcIQqLkZG9VBvU2f1eNhMfVaFuIGxgq_FaWv8WIxHj1J1OpvxIOj8fs0IUiFlQ0Q0PBz4GRrFNIudwsrPHVjk2Ckg-fsoCOVvBcHgJ7fGGyBlhSTGTvWC_3-JcbsU=\",\n" +
				"\t\t\t\"facilitated_booking_available\": false,\n" +
				"\t\t\t\"pnr_count\": 1,\n" +
				"\t\t\t\"has_airport_change\": false,\n" +
				"\t\t\t\"technical_stops\": 0,\n" +
				"\t\t\t\"throw_away_ticketing\": false,\n" +
				"\t\t\t\"hidden_city_ticketing\": false,\n" +
				"\t\t\t\"virtual_interlining\": false,\n" +
				"\t\t\t\"local_arrival\": \"2023-06-01T08:20:00.000Z\",\n" +
				"\t\t\t\"utc_arrival\": \"2023-06-01T06:20:00.000Z\",\n" +
				"\t\t\t\"local_departure\": \"2023-06-01T07:15:00.000Z\",\n" +
				"\t\t\t\"utc_departure\": \"2023-06-01T05:15:00.000Z\"\n" +
				"\t\t}\n" +
				"\t],\n" +
				"\t\"_results\": 1,\n" +
				"\t\"search_params\": {\n" +
				"\t\t\"flyFrom_type\": \"airport\",\n" +
				"\t\t\"to_type\": \"airport\",\n" +
				"\t\t\"seats\": {\n" +
				"\t\t\t\"passengers\": 1,\n" +
				"\t\t\t\"adults\": 1,\n" +
				"\t\t\t\"children\": 0,\n" +
				"\t\t\t\"infants\": 0\n" +
				"\t\t}\n" +
				"\t},\n" +
				"\t\"all_stopover_airports\": [],\n" +
				"\t\"sort_version\": 0\n" +
				"}";
		//when
		resultList = methodBase.readJson(json);
		//then
		Assertions.assertThat(resultList.get(0).getPirce()).isEqualTo(21.0f);
		//Assertions.assertThat(resultList.get(0).getCityFrom()).isEqualTo("Gdańsk");
		//Assertions.assertThat(resultList.get(0).getCityTo()).isEqualTo("Wrocław");
		Assertions.assertThat(resultList.get(0).getFlyFrom()).isEqualTo("GDN");
		Assertions.assertThat(resultList.get(0).getFlyTo()).isEqualTo("WRO");
	}
	@Test
	public void readJson_negaive() throws JsonProcessingException {
		//given
		List<Flights> resultList = new ArrayList<>();
		String json = "{ \\\"name\\\": }";

		//when
		//resultList = methodBase.readJsonTest(json);

		//then
		// Sprawdzenie, czy metoda rzuca wyjątek NoSuchElementException
		assertThrows(JsonProcessingException.class, () -> {
			methodBase.readJson(json);
		});

		// Sprawdzenie, czy wyjątek ma odpowiednią treść
		Exception exception = assertThrows(JsonProcessingException.class, () -> {
			methodBase.readJson(json);
		});
		System.out.println(exception.getMessage());

	}

	@Test
	public void timeConversionTest() {
		//given
		String time = "01/07/2023";
		//when
		Long unixTime = methodBase.timeConversion(time);
		//then
		org.assertj.core.api.Assertions.assertThat(unixTime).isEqualTo(1688166000L); //2023/07/01 01:00:00
		//https://www.unixtimestamp.com/
	}
	@Test
	public void converterMethodTest() {
		//given
		int input = 1693695600;
		String expectedResult = "03/09/2023";
		//when
		String resultDate = methodBase.converterMethod(input);
		//then
		Assertions.assertThat(expectedResult).isEqualTo(resultDate);

	}
//	@Test
//	public void getMinIndexesTest() {
//		//given
//		float [] tab = new float[4];
//		tab[0] = 48;
//		tab[1] = 0;
//		tab[2] = 0;
//		tab[3] = 48;
//		float[] resultTab = new float[4];
//		//when
//		resultTab = methodBase.getMinIndexes(tab);
//		//then
//		for(int i=0;i<resultTab.length;i++) {
//			System.out.println("index no: " + i + " = " + resultTab[i]);
//		}
//		System.out.println(resultTab.length);
//
//	}


}
