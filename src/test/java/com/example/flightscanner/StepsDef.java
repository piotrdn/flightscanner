package com.example.flightscanner;

import com.example.flightscanner.Repository.FlightScannerRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class StepsDef {

    MethodBase methodBase;
    private FlightScannerRepository flightScannerRepository;


    Long result;
    @Given("I want to get UnixTimestamp")
    public void i_want_to_get_UnixTimestamp() {
        methodBase = new MethodBase(flightScannerRepository);
    }
    @When("I put {string}")
    public void i_put(String string) {
        result = methodBase.timeConversion(string);
    }
    @Then("result is {long}")
    public void result_is(Long long1) {
        Assertions.assertEquals(long1, result);
    }




    String result2;
    @Given("I want to get date")
    public void i_want_to_get_date() {
        methodBase = new MethodBase(flightScannerRepository);
    }
    @When("I put unixTimestamp {int}")
    public void i_put_unixTimestamp(Integer int2) {
        result2 = methodBase.converterMethod(int2);
    }
    @Then("my result is {string}")
    public void my_result_is(String string) {
        Assertions.assertEquals(string, result2);
    }



    String result3;
    @Given("I test the achieveJson method")
    public void i_test_the_achieve_Json() {
        methodBase = new MethodBase(flightScannerRepository);
    }
    @When("I put the url: {string}")
    public void i_put_the_url(String url) {
        result3 = methodBase.achieveJson(url);
    }
    @Then("I want to get json: {string}")
    public void i_want_to_get_json(String string) {
        org.assertj.core.api.Assertions.assertThat(string).contains("\"flyFrom\": \"GDN\"");
        org.assertj.core.api.Assertions.assertThat(string).contains("\"flyTo\": \"WRO\"");
    }



}
