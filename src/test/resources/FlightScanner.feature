Feature: FlightScanner test
  Scenario: timeConversion test
    Given I want to get UnixTimestamp
    When I put "01/07/2023"
    Then result is 16881660001

  Scenario: converterMethod test
    Given I want to get date
    When I put unixTimestamp 1690930800
    Then my result is "02/08/2023"

    Scenario: achieveJson test
      Given I test the achieveJson method
      When I put the url: "https://api.tequila.kiwi.com/v2/search?fly_from=GDN&fly_to=WRO&date_from=01/06/2023&date_to=01/06/2023&flight_type=oneway&ret_from_diff_city=false&ret_to_diff_city=false&adults=1&only_working_days=false&only_weekends=false&curr=EUR&locale=pl&vehicle_type=aircraft&limit=5&max_stopovers=0"
      Then I want to get json: "\"flyFrom\": \"GDN\", \"flyTo\": \"WRO\""